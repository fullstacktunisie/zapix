package com.fullstack.zapix.model;

public class SliderItem {
    private int background;
    private String title;
    private String description;


    public SliderItem(int background, String title, String description) {
        this.background = background;
        this.title = title;
        this.description = description;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

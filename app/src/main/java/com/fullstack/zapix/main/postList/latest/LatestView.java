package com.fullstack.zapix.main.postList.latest;

import android.view.View;

import com.fullstack.zapix.main.base.BaseFragmentView;
import com.fullstack.zapix.model.Post;

public interface LatestView  extends BaseFragmentView {
    void hideCounterView();
    void openPostDetailsActivity(Post post, View v);
    void showCounterView(int count);
    void showFloatButtonRelatedSnackBar(int messageId);
    void refreshPostList();
    void removePost();
    void updatePost();

}

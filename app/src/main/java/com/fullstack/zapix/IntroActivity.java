package com.fullstack.zapix;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.fullstack.zapix.adapters.SliderAdapter;
import com.fullstack.zapix.main.main.MainActivity;

public class IntroActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_intro);

        ViewPager viewPager = findViewById(R.id.viewPager);
        TabLayout indicator = findViewById(R.id.indicator);

        viewPager.setAdapter(new SliderAdapter(this, Constants.sliderItemsList()));
        indicator.setupWithViewPager(viewPager, true);
        findViewById(R.id.get_started).setOnClickListener(v -> {
            startActivity(new Intent(IntroActivity.this, MainActivity.class));
            finish();
        });
    }
}

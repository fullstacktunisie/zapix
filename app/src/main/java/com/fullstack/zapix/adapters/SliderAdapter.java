package com.fullstack.zapix.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fullstack.zapix.R;
import com.fullstack.zapix.model.SliderItem;

import java.util.ArrayList;

public class SliderAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<SliderItem> sliderItems;

    public SliderAdapter(Context context, ArrayList<SliderItem> sliderItems) {
        this.context = context;
        this.sliderItems = sliderItems;
    }

    @Override
    public int getCount() {
        return sliderItems.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        SliderItem sliderItem = sliderItems.get(position);
        View view = LayoutInflater.from(context).inflate(R.layout.item_slider, container, false);

        ImageView image = view.findViewById(R.id.item_img);
        TextView title = view.findViewById(R.id.item_title);
        TextView description = view.findViewById(R.id.item_description);

        title.setText(sliderItem.getTitle());
        description.setText(sliderItem.getDescription());
        image.setImageResource(sliderItem.getBackground());

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}

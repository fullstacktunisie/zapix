package com.fullstack.zapix.adapters;

import android.support.v7.widget.RecyclerView;

import com.fullstack.zapix.main.base.BaseFragment;
import com.fullstack.zapix.managers.PostManager;
import com.fullstack.zapix.managers.listeners.OnPostChangedListener;
import com.fullstack.zapix.model.Post;
import com.fullstack.zapix.utils.LogUtil;

import java.util.LinkedList;
import java.util.List;

public abstract class BaseFragmentPostsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String TAG = BasePostsAdapter.class.getSimpleName();

    protected List<Post> postList = new LinkedList<>();
    protected BaseFragment activity;
    protected int selectedPostPosition = RecyclerView.NO_POSITION;

    public BaseFragmentPostsAdapter(BaseFragment activity) {
        this.activity = activity;
    }

    protected void cleanSelectedPostInformation() {
        selectedPostPosition = -1;
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return postList.get(position).getItemType().getTypeCode();
    }

    protected Post getItemByPosition(int position) {
        return postList.get(position);
    }

    private OnPostChangedListener createOnPostChangeListener(final int postPosition) {
        return new OnPostChangedListener() {
            @Override
            public void onObjectChanged(Post obj) {
                postList.set(postPosition, obj);
                notifyItemChanged(postPosition);
            }

            @Override
            public void onError(String errorText) {
                LogUtil.logDebug(TAG, errorText);
            }
        };
    }

    public void updateSelectedPost() {
        if (selectedPostPosition != RecyclerView.NO_POSITION) {
            Post selectedPost = getItemByPosition(selectedPostPosition);
            PostManager.getInstance(activity.getContext()).getSinglePostValue(selectedPost.getId(), createOnPostChangeListener(selectedPostPosition));
        }
    }
}
